package com.lion.exam.catalog.model.api;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {
  private static final long serialVersionUID = 1L;

  private String brandId;
  private String productId;
  private String priceList;
  private Double price;
  private LocalDateTime startDate;
  private LocalDateTime endDate;

}
