package com.lion.exam.catalog.model.api;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest implements Serializable {
  private static final long serialVersionUID = 1L;

  private String brandId;
  private String productId;
  private LocalDateTime applicationDate;

}
