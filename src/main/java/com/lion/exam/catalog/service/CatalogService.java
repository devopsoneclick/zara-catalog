package com.lion.exam.catalog.service;

import com.lion.exam.catalog.model.api.Catalog;
import com.lion.exam.catalog.model.api.Product;
import com.lion.exam.catalog.model.api.ProductRequest;
import com.lion.exam.catalog.model.entity.CatalogEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CatalogService {

  Flux<Catalog> findAllCatalogs();

  Mono<Product> retrieveProduct(ProductRequest request);

  Mono<Void> saveCatalog(CatalogEntity catalogEntity);

  Mono<Void> updateCatalog(CatalogEntity catalogEntity);

  Mono<Void> deleteCatalog(CatalogEntity catalogEntity);
}
