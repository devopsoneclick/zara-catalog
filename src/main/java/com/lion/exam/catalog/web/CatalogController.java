package com.lion.exam.catalog.web;

import com.lion.exam.catalog.model.api.Catalog;
import com.lion.exam.catalog.model.api.Product;
import com.lion.exam.catalog.model.api.ProductRequest;
import com.lion.exam.catalog.service.CatalogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/api/zara/v1")
@RequiredArgsConstructor
@Slf4j
public class CatalogController {

  private final CatalogService catalogService;

  @GetMapping("/catalog")
  private Flux<Catalog> getAllCatalog() {
    return catalogService.findAllCatalogs();
  }

  @PostMapping("/catalog/product")
  private Mono<Product> getProduct(@RequestBody ProductRequest request) {
    return catalogService.retrieveProduct(request);
  }
}
