package com.lion.exam.catalog.component;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class ApplicationProperties {

  @Value("${application.csv.path}")
  private String csvPath;

}
