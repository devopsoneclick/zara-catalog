package com.lion.exam.catalog.scheduler;

import com.lion.exam.catalog.component.ApplicationProperties;
import com.lion.exam.catalog.helper.Constant;
import com.lion.exam.catalog.mapper.CatalogMapper;
import com.lion.exam.catalog.model.csv.CatalogCsv;
import com.lion.exam.catalog.service.CatalogService;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
@Slf4j
public class CatalogScheduler {

  private final ApplicationProperties properties;
  private final CatalogMapper catalogMapper;
  private final CatalogService catalogService;

  @Scheduled(fixedRateString = "${application.scheduler.period-milliseconds}")
  public void priceLoadingTask() {
    CsvToBean<CatalogCsv> csvBuilt = new CsvToBeanBuilder<CatalogCsv>(this.getReader())
        .withType(CatalogCsv.class)
        .withSkipLines(1)
        .withSeparator(',')
        .withIgnoreLeadingWhiteSpace(true)
        .build();
    var catalogList = csvBuilt.parse();
    Flux.fromIterable(catalogList)
        .filter(this::validateUpgrade)
        .flatMap(this::upgradeCatalog)
        .subscribe();
  }

  private Reader getReader() {
    BufferedReader reader = null;
    try {
      FileReader fileReader = new FileReader(properties.getCsvPath());
      reader = new BufferedReader(fileReader);
    } catch (FileNotFoundException ex) {
      log.error("Error al leer ruta: ", ex);
    }
    return reader;
  }

  private Mono<Void> upgradeCatalog(CatalogCsv csvBean) {
    var entity = catalogMapper.toCatalogEntity(csvBean);
    switch (csvBean.getStatus()) {
      case Constant.INSERT: return catalogService.saveCatalog(entity);
      case Constant.UPDATE: return catalogService.updateCatalog(entity);
      case Constant.DELETE: return catalogService.deleteCatalog(entity);
    }
    return Mono.empty();
  }

  private boolean validateUpgrade(CatalogCsv csvBean) {
    return Constant.INSERT.equals(csvBean.getStatus())
        || Constant.UPDATE.equals(csvBean.getStatus())
        || Constant.DELETE.equals(csvBean.getStatus());
  }
}
