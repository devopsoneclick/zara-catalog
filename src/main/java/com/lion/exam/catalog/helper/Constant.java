package com.lion.exam.catalog.helper;

public class Constant {

  private Constant() {
  }

  public final static String INSERT =  "1";
  public final static String UPDATE =  "2";
  public final static String DELETE =  "-1";
}
